package com.eliasnogueira.credito.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Simulacao {

    private int id;
    private String nome;
    private String cpf;
    private String email;
    private float valor;
    private int parcelas;
    private Boolean seguro;
}
